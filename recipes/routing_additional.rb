#######################
##Initial sanity checks 
#Don't run if the right info isn't already set
if node['vlan'][0]['vlanid'].nil? || node['vlan'][0]['gateway'].nil? || node['vlan'][0]['network'].nil?
  abort("Chef environment attributes not set")
end


##########
##Includes
require 'fileutils'
require 'open3'

###########
##Variables
baseip, stderr, status=Open3.capture3("hostname -i")
vlan = node['vlan'][0]['vlanid']
net_dir = '/etc/sysconfig/network-scripts/'
#Environment and other attributes used in templates
#node['vlan'][0]['vlanid']
#node['vlan'][0]['gateway']
#node['vlan'][0]['network']
#node['vlan'][0]['add_route']


##############
##Main Section


template "#{net_dir}route-bond0.#{vlan}" do
  action :create
  source 'route-bond.vlan.erb'
  mode '0644'
  owner 'root'
  group 'root'
  variables({ :ipaddr => "#{baseip}" })
end

template "#{net_dir}rule-bond0.#{vlan}" do
  action :create
  source 'rule-bond.vlan.erb'
  mode '0644'
  owner 'root'
  group 'root'
  variables({ :ipaddr => "#{baseip}" })
end


