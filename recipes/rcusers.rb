#
# Cookbook Name:: hadoop
# Recipe:: cr_user_grps.rb
#
# Copyright 2016, General Motors
#
# All rights reserved - Do Not Redistribute
#

# This recipe is layed out by first creating the group followed by the creation of the user.
# At the bottom of the recipe all of the users that need to be in the hadoop group are all added.
# I also note which users are not included in the hadoop group

# This is needed if assigning encrypted passwords
# This recipe does not assign passwords to the accounts as these are all service accounts
gem_package "ruby-shadow" do 
  action :install
end

# Create a unix group
group "hdfs" do
     gid 40001
end
 
# Create a unix user
user "hdfs" do
     comment "Hadoop hdfs user"
     uid 40001
     gid "hdfs"
     home "/home/hdfs"
     shell "/bin/bash"
end

# Create a unix group
group "mapred" do
     gid 40002
end

# Create a unix user
user "mapred" do
     comment "Hadoop mapred user"
     uid 40002
     gid "mapred"
     home "/home/mapred"
     shell "/bin/bash"
end

# Create a unix group
group "kafka" do
     gid 40003
end

# Create a unix user
user "kafka" do
     comment "Hadoop kafka user"
     uid 40003
     gid "kafka"
     home "/home/kafka"
     shell "/bin/bash"
end

# Create a unix group
group "rrdcached" do
     gid 40004
end

# Create a unix user
user "rrdcached" do
     comment "Hadoop rrdcached user"
     uid 40004
     gid "rrdcached"
     home "/home/rrdcached"
     shell "/bin/bash"
end

# Create a unix group
group "zookeeper" do
     gid 40005
end

# Create a unix user
user "zookeeper" do
     comment "Hadoop zookeeper user"
     uid 40005
     gid "zookeeper"
     home "/home/zookeeper"
     shell "/bin/bash"
end

# Create a unix group
group "hbase" do
     gid 40006
end

# Create a unix user
user "hbase" do
     comment "Hadoop hbase user"
     uid 40006
     gid "hbase"
     home "/home/hbase"
     shell "/bin/bash"
end

# Create a unix group
group "hive" do
     gid 40007
end

# Create a unix user
user "hive" do
     comment "Hadoop hive user"
     uid 40007
     gid "hive"
     home "/home/hive"
     shell "/bin/bash"
end

# Create a unix group
group "oozie" do
     gid 40008
end

# Create a unix user
user "oozie" do
     comment "Hadoop oozie user"
     uid 40008
     gid "oozie"
     home "/home/oozie"
     shell "/bin/bash"
end

# Create a unix group
group "ambari" do
     gid 40009
end

# Create a unix user
user "ambari" do
     comment "Hadoop ambari user"
     uid 40009
     gid "ambari"
     home "/home/ambari"
     shell "/bin/bash"
end

# Create a unix group
group "ambari-qa" do
     gid 40010
end

# Create a unix user
user "ambari-qa" do
     comment "Hadoop ambari-qa user"
     uid 40010
     gid "ambari-qa"
     home "/home/ambari-qa"
     shell "/bin/bash"
end

# Create a unix group
group "ams" do
     gid 40011
end

# Create a unix user
user "ams" do
     comment "Hadoop ams user"
     uid 40011
     gid "ams"
     home "/home/ams"
     shell "/bin/bash"
end

# Create a unix group
group "cassandra" do
     gid 40012
end

# Create a unix user
user "cassandra" do
     comment "Hadoop cassandra user"
     uid 40012
     gid "cassandra"
     home "/home/cassandra"
     shell "/bin/bash"
end

# Create a unix group
group "flume" do
     gid 40013
end

# Create a unix user
user "flume" do
     comment "Hadoop flume user"
     uid 40013
     gid "flume"
     home "/home/flume"
     shell "/bin/bash"
end

# Create a unix group
group "hcat" do
     gid 40014
end

# Create a unix user
user "hcat" do
     comment "Hadoop hcat user"
     uid 40014
     gid "hcat"
     home "/home/hcat"
     shell "/bin/bash"
end

# Create a unix group
group "knox" do
     gid 40015
end

# Create a unix user
user "knox" do
     comment "Hadoop knox user"
     uid 40015
     gid "knox"
     home "/home/knox"
     shell "/bin/bash"
end

# Create a unix group
group "solr" do
     gid 40016
end

# Create a unix user
user "solr" do
     comment "Hadoop solr user"
     uid 40016
     gid "solr"
     home "/home/solr"
     shell "/bin/bash"
end

# Create a unix group
group "spark" do
     gid 40017
end

# Create a unix user
user "spark" do
     comment "Hadoop spark user"
     uid 40017
     gid "spark"
     home "/home/spark"
     shell "/bin/bash"
end

# Create a unix group
group "sqoop" do
     gid 40018
end

# Create a unix user
user "sqoop" do
     comment "Hadoop sqoop user"
     uid 40018
     gid "sqoop"
     home "/home/sqoop"
     shell "/bin/bash"
end

# Create a unix group
group "yarn" do
     gid 40019
end

# Create a unix user
user "yarn" do
     comment "Hadoop yarn user"
     uid 40019
     gid "yarn"
     home "/home/yarn"
     shell "/bin/bash"
end

# Create a unix group
group "postgres" do
     gid 40020
end

# Create a unix user
user "postgres" do
     comment "Hadoop postgres user"
     uid 40020
     gid "postgres"
     home "/var/lib/pgsql"
     shell "/bin/bash"
end

# Create a unix group
group "ranger" do
     gid 40021
end

# Create a unix user
user "ranger" do
     comment "Hadoop ranger user"
     uid 40021
     gid "ranger"
     home "/home/ranger"
     shell "/bin/bash"
end

# Users not in the hadoop group that are created in this recipe
#   postgres

# Create the unix group hadoop and add all of the relevant users to this group
group "hadoop" do
  action :create
  gid 40000
  members ['hdfs','mapred','kafka','zookeeper','hbase','hive','oozie','ambari','ambari-qa','ams','cassandra','flume','hcat','knox','solr','spark','sqoop','yarn','ranger']
end

# Create a unix user
user "hadoop" do
     comment "Hadoop hadoop user"
     uid 40000
     gid "hadoop"
     home "/home/hadoop"
     shell "/bin/bash"
end

