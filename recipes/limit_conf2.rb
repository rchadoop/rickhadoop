template '/etc/security/limits.conf' do
  source 'limits_conf.erb'
  mode '0644'
  owner 'root'
  group 'root'
end
