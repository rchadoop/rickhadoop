#
# Cookbook Name:: hadoop
# Recipe:: sysctl_conf2
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
# This recipe installes the default sysctl.conf file, 
# makes sure the bridge module is installed and 
# reloads sysctl settings. 
template '/etc/sysctl.conf' do
  source 'sysctl.erb'
  mode '0644'
  owner 'root'
  group 'root'
end

execute "bridge modprobe" do 
  action :run
  command "/sbin/modprobe -v bridge" 
end 

execute "sysctl" do 
  action :run
  command "/sbin/sysctl -p" 
end 
