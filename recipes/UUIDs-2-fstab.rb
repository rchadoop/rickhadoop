#!/opt/chef/embedded/bin/ruby

require 'fileutils'
require 'open3'
mydate, stderr, status = Open3.capture3("date +%m%d%y_%H%M%S")

disk_string=2000
min_number_disks=11

# backup fstab
# check fstab
# make sure more than 11 disks
# 	sort and do a mkfs
# 	make directorys
# 	update fstab
# 	mount -a
#--------------------------------------------------------------------------
# Backup /etc/fstab 

fstab_copy,stderr,status = Open3.capture3("cp /etc/fstab /etc/fstab_#{mydate} ")
#puts  "fstab_copy successul" + stderr

#--------------------------------------------------------------------------

yum_xfsprogs, stderr, status = Open3.capture3('yum --nogpgcheck -y install xfsprogs')

if File.exist?("/sbin/mkfs.xfs")
  puts " UU got /sbin/mkfs.xfs"
else
  puts " UU - NO mkfs.xfs YET"
  sleep(40)
end
  puts "after sleep"



#if [ "$(grep -c "/disk-" /etc/fstab)" = "0" ] 

disks_in_fstab, stderr, status = Open3.capture3('grep -c "/disk-" /etc/fstab')
        unless disks_in_fstab.to_i  >= 1
#        puts "disk in fstab : " + disks_in_fstab



#Populate Good Disc Array
#
mydisks, stderr, status = Open3.capture3("fdisk -l | grep dev/sd")
gooddisks=[]
#baddisks[]

mydisks.each_line do |line|
        disk_name, disk_size = line.split(' ').values_at(1, 2)
        if disk_size.to_i >= disk_string
          #unless (line.include?('Linux') || line.include?('sdi'))
          unless (line.include?('Linux'))
          gooddisks.push(disk_name.chomp(':'))
	  end
      end
end

#--------------------------------------------------------------------------
#if gooddisks.length >  min_number_disks
  #puts "Have greater than " + min_number_disks.to_s  + gooddisks.length.to_s + "disks"
#  puts "Have greater than " + min_number_disks.to_s  + " disks. Have " + gooddisks.length.to_s + " disks"
#end
      sorted_gooddisks=gooddisks.sort
      disknum=0
#---------------------------------------------------------------
  sorted_gooddisks.each do |gooddisk|
          disknum=disknum + 1
         fork do
         #puts "here : gooddisk = " + gooddisk
         # puts "Running : mkfs.xfs -f " + gooddisk + " > /dev/null "
          #fork_stdout,stderr,status = Open3.capture3("mkfs.xfs -f " + gooddisk )
          fork_stdout,stderr,status = Open3.capture3("/sbin/mkfs.xfs -f " + gooddisk )
          puts "After Running : mkfs.xfs -f " + gooddisk + " > /dev/null "
	#	     fork_stdout.each_line do |fline|
	#             puts "fline = " + fline
 	#	     end
	  uu,stderr,status=Open3.capture3("blkid " + gooddisk + "| awk '{ print $2 }'")
		uu.each_line do |uuline|
	 	#puts  uuline.chomp() + "     /disk-" + disknum.to_s + "        xfs     noatime,nodev   0 0"
		  open('/etc/fstab', 'a') { |f|
                  f.puts uuline.chomp() + "     /disk-" + disknum.to_s + "        xfs     noatime,nodev   0 0"
                  }
	        
		mkdir_out,stderr,status=Open3.capture3("mkdir  /disk-" + disknum.to_s)
 	        end
		mount_out,stderr,status=Open3.capture3("/bin/mount -a")
         end
		mount_out,stderr,status=Open3.capture3("/bin/mount -a")
  end
#---------------------------------------------------------------

#puts "Last Line"
end  #end for unless disks in fstab


