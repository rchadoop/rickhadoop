#
# Cookbook Name:: hadoop
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
ruby_block "Rename_motd" do
  block do
   if File.exist?("/etc/motd")
       ::File.rename("/etc/motd","/etc/motd.hadoop")
	end   
   if File.exist?("/etc/issue")
       ::File.rename("/etc/issue","/etc/issue.hadoop")
	end   
  end
end
