#
# Cookbook Name:: hadoop
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
ruby_block "Change umask to 022" do
  block do
# file /etc/login.defs  
    fe = Chef::Util::FileEdit.new("/etc/login.defs")
	fe.search_file_replace(/UMASK           077/,
	                       "UMASK           022")
    fe.write_file
#	
#   file /etc/bashrc
    fe = Chef::Util::FileEdit.new("/etc/bashrc")
	fe.search_file_replace(/umask 077/,
	                       "umask 022")
    fe.write_file

#   file /etc/profile
    fe = Chef::Util::FileEdit.new("/etc/profile")
	fe.search_file_replace(/umask 077/,
	                       "umask 022")
    fe.write_file

#   file /etc/skel/.bashrc 
    fe = Chef::Util::FileEdit.new("/etc/skel/.bashrc")
	fe.search_file_replace(/umask 077/,
	                       "umask 022")
    fe.write_file

	Dir.glob('/home/*/.bashrc') do |rc_file|
	fe = Chef::Util::FileEdit.new(rc_file)
	fe.search_file_replace(/umask 077/,
	                       "umask 022")
    fe.write_file
	end
  end
end
  
