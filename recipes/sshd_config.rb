#
# Cookbook Name:: hadoop
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
ruby_block "Add sshd entry MaxStartup=1024" do
  block do
    fe = Chef::Util::FileEdit.new("/etc/ssh/sshd_config")
    fe.insert_line_if_no_match(/Added by chef for hadoop/,
	                           "# Added by chef for hadoop")
	fe.insert_line_if_no_match(/MaxStartups 1024/,			   
                               "MaxStartups 1024")
    fe.write_file
  end
end
