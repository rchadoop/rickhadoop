# Cookbook Name::hadoop
# Recipe:: test
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
mgmt_string = node['mgmt_access_conf_string']

File.open("/tmp/access.conf", "r").each_line do |line|
  if line =~ /- : ALL : ALL/
     puts "+ : root : #{mgmt_string}"
     puts "+ : root : " + mgmt_string
     puts "Hello, how are you ?"
  end
  puts "#{line}"
end
