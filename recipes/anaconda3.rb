#
# Cookbook Name:: hadoop
# Recipe:: anaconda3
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
# This recipe installes the default sysctl.conf file, 
# makes sure the bridge module is installed and 
# reloads sysctl settings. 
template '/etc/yum.repos.d/anaconda.repo' do
  source 'anaconda_repo.erb'
  mode '0644'
  owner 'root'
  group 'root'
end

yum_package "anaconda3"  do 
       options '--nogpgcheck' 
     end

yum_package "anaconda3-modules"  do 
       options '--nogpgcheck' 
     end
