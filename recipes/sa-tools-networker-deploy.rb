#
# Cookbook Name:: hadoop
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
bash 'networker-deploy' do
  user 'root'
  cwd '/root/bin'
  code <<-EOH
  if [ ! -x /usr/sbin/nsrexecd ] ; then
  
     cd /root/bin
     echo "/root/bin/sa-tools networker-deploy install" >> networker-test
     /root/bin/sa-tools networker-deploy install
  fi
  EOH
end