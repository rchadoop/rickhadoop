# Cookbook Name::hadoop
# Recipe:: dsh_group
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
# This recipe create 3 files contating lists of servers
# datahosts mgmthosts and all hosts
directory '/root/.ssh' do
  mode 0700
  owner 'root'
  group 'root'
  action :create
  recursive true
end


template '/root/.ssh/authorized_keys' do
  source 'vdp_prod_pub_key'
end



