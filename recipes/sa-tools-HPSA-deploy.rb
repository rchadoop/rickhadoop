#
# Cookbook Name:: hadoop
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
bash 'HPSA-deploy' do
  user 'root'
  cwd '/root/bin'
  code <<-EOH	
  if [ ! -x /opt/opsware/agent/bin/python ] ; then

     ZONE=$(hostname | cut -c 4)
       case $ZONE in
           i) HPSA_ARG1=int ;;
           x) HPSA_ARG1=ext ;;
           s) HPSA_ARG1=sec ;;
           c) HPSA_ARG1=pci ;;
           b) HPSA_ARG1=bpo ;;
           *)
       esac
	
     ENV=$(hostname | cut -c 5)
     case $ENV in
        p) HPSA_ARG2=pd ;;
        x) HPSA_ARG2=pr ;;
        t) HPSA_ARG2=tt ;;
        d|l) HPSA_ARG2=dv ;;
        *)
    esac  
		   	   
   # sat (satellite)
   # wrn - Warren
   # mil - Milford
   # hho - Australia
   # pdg - China
   # bph - Korea
   # scs - Brazil N/a
   # rus - Germany
   # kmp - South Africa n/a
   # bga - India
  SAT=$(hostname | cut -c 1-3)
    case $SAT in
        dcw) HPSA_ARG3=wrn ;;	# warren			
        epg|dcm) HPSA_ARG3=mil ;; # milford
        fbd) HPSA_ARG3=hho ;; # australia
        pdg) HPSA_ARG3=pdg ;; # China
        bph) HPSA_ARG3=bph ;; # Korea			
        rus) HPSA_ARG3=rus ;; # Germany
        bga) HPSA_ARG3=bga ;; # India			
	esac

	#

  cd /root/bin
  /root/bin/sa-tools HPSA-deploy $HPSA_ARG1 $HPSA_ARG2 $HPSA_ARG3
  fi
  EOH
end