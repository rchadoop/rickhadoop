#
# Cookbook Name:: hadoop
# Recipe:: jumbo_frame_fix.rb
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
ruby_block "Add entry from b MTU=9600 to ifcfg-bond0" do
  block do
    fe = Chef::Util::FileEdit.new("/etc/sysconfig/network-scripts/ifcfg-eth0")
    fe.insert_line_if_no_match(/MTU=9000/,			   
                               "#b's stuff")
    fe.write_file
  end
  notifies :restart, 'service[network]'
end
 puts "before network in b"


service 'network' do
  action [:nothing]
#  not_if 'ifconfig eth0 |grep MTU:9000'
end


 puts "after network in b, sleeping 20 before c"
sleep(2)
