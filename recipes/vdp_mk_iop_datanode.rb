# Previously  mk_fs_hadoop.rb
# This recipe creates  /hadoop (100G), /usr/iop (20G), /var/iop (100G)
# This recipe is executed on Management Nodes Only
#
bash 'mk_hadoop_fs' do
  user 'root'
  cwd '/tmp'
  code <<-EOH

#    sa-tools creatfs-deploy -vg vgapp -d /dev/sdb   

#   if [ $(awk '{ print $2 }'  /etc/fstab |grep -c \/hadoop) = 0 ] ; then
#      cd /root/bin
#          /root/bin/sa-tools createfs-deploy -vg vgapp -lv appvol -s 100 -m /hadoop -d /dev/sdb
#   fi
#
#
   if [ $(awk '{ print $2 }'  /etc/fstab |grep -c \/usr/iop) = 0 ] ; then
      cd /root/bin
          /root/bin/sa-tools createfs-deploy -vg vgos -lv usriopvol -s 20 -m /usr/iop 
   fi
#
#
   if [ $(awk '{ print $2 }'  /etc/fstab |grep -c \/var/iop) = 0 ] ; then
      cd /root/bin
          /root/bin/sa-tools createfs-deploy -vg vgos -lv variopvol  -s 100 -m /var/iop
   fi
  EOH
end


