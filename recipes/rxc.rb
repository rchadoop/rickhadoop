#!/opt/chef/embedded/bin/ruby

require 'fileutils'
require 'open3'
#mydate, stderr, status = Open3.capture3("date +%m%d%y_%H%M%S")

puts "Checking interface MTU"

ifconfig_output, stderr, status = Open3.capture3('ifconfig eth0 |grep -i mtu')


ifconfig_output.each_line do |line|
        unless (line.include?('9000'))
	  puts "not set" + line
	else
	  puts "set" + line
	end
end
