template '/etc/sysctl.conf' do
  source 'sysctl.erb'
  mode '0644'
  owner 'root'
  group 'root'
end

execute "sysctl" do 
  command "/sbin/sysctl -p" 
end 
