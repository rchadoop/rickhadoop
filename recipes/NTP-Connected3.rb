#
# Cookbook Name:: hadoop
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
bash 'NTP_Check' do
  user 'root'
  cwd '/tmp'
  code <<-EOH
  ntpq -p | grep -ic ^\*
  EOH
end