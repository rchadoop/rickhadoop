#
# Cookbook Name:: hadoop
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
bash 'OS-patch-deploy' do
  user 'root'
  cwd '/root/bin'
  code <<-EOH
  # set the PATCHLVL
  PATCHLVL="2016Q1" 

  cd /root/bin
  /root/bin/sa-tools OS-patch-deploy -p $PATCHLVL reboot
  EOH
end
