#
# Cookbook Name:: hadoop
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
#
# This is the package which allows you to use mkfs.xfs command.
#  We create the datanode drives as
# xfs
package 'xfsprogs'
