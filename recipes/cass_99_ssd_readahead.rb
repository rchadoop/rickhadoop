template '/etc/udev/rules.d/99-ssd.rules' do
  source 'cass_99_ssd_readahead.erb'
  mode '0644'
  owner 'root'
  group 'root'
end

