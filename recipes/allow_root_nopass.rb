#
# Cookbook Name:: hadoop
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
bash 'user-grp-add' do
  user 'root'
  cwd '/tmp'
  code <<-EOH
   mkdir -p /root/.ssh

   echo "Running sshd recipe"

if [ `awk '{ print $1 " " $2 }'  /etc/ssh/sshd_config | grep -c 'PermitRootLogin without-password' `  = 0 ]
then

   echo "Configuring Root Login without password .... restarting sshd"
   #Create a backup and change permitRootLogin setting to without-password, in order to allow passwordless ssh for root.
   cp -p /etc/ssh/sshd_config /etc/ssh/sshd_config_backup
   sed -i 's/PermitRootLogin no/PermitRootLogin without-password/' /etc/ssh/sshd_config
   
   # restart sshd service
   echo "Configured Root Login, restarting sshd " >> /var/chef/sshd_config.out
   service sshd restart
   
#   This section may not be relevant or need changing for real security configuration. 
#   Create a backup and change access permissions to stop denying all users unless they fit criteria
#   cp /etc/security/access.conf /etc/security/access.conf_backup
#   sed -i 's/- : ALL : ALL/#- : ALL : ALL/' /etc/security/access.conf

else
   echo "Root Login already configured" >> /var/chef/sshd_config.out
fi
  EOH
end
