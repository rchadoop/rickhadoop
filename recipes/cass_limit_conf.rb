template '/etc/security/limits.conf' do
  source 'cass_limits_conf.erb'
  mode '0644'
  owner 'root'
  group 'root'
end
