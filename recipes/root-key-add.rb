#
# Cookbook Name:: hadoop
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
bash 'user-grp-add' do
  user 'root'
  cwd '/tmp'
  code <<-EOH
   mkdir -p /root/.ssh
#   RSA="ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA2GK0OoB/wietjpoPDnAGQ8+1VmXtWAZtBTsdK3RCER0fJ9ob5h+h02lZnYNmm36UIFx3GVyk/DOkreHBwz1r6hcQrxBIGSGPtXRV8VNAansxqVdJaesM2/CZzE32B5Nr09OT95Kd8Dft11rwBtFmMw+qIq84MLJF9qHthUgY74SbQ+iKviJ/Y7UkH2aevxMnUjLb47oWAQo6lKPwMw79YNJlEx+T/hnyHZh1FlAYWRiP68a26UqUYfYCKywsaV87EIqmLuJg7PgT54gn4wqHVN2QuJxk4l3ItuPQmFHtml9A3eG/KK6iEZUXmPWJAsA9klEEpaTrlULsYlRRTMnv3w== "
#   if [[ ! -f  /root/.ssh/authorized_keys ]] ; then
#      echo $RSA  >> /root/.ssh/authorized_keys
#   fi
 
   #Create a backup and change permitRootLogin setting to without-password, in order to allow passwordless ssh for root.
   cp /etc/ssh/sshd_config /etc/ssh/sshd_config_backup
   sed -i 's/PermitRootLogin no/PermitRootLogin without-password/' /etc/ssh/sshd_config
   
   # restart sshd service
   service sshd restart
   
   ###This section may not be relevant or need changing for real security configuration. 
   #Create a backup and change access permissions to stop denying all users unless they fit criteria
   cp /etc/security/access.conf /etc/security/access.conf_backup
   #sed -i 's/- : ALL : ALL/#- : ALL : ALL/' /etc/security/access.conf

  EOH
end
