#
# Cookbook Name:: hadoop
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
bash 'trans-huge-pg' do
  user 'root'
  cwd '/tmp'
  code <<-EOH
  if test -f /sys/kernel/mm/redhat_transparent_hugepage/enabled; then
     echo never > /sys/kernel/mm/redhat_transparent_hugepage/enabled
  fi
  if test -f /sys/kernel/mm/redhat_transparent_hugepage/defrag; then
     echo never > /sys/kernel/mm/redhat_transparent_hugepage/defrag
  fi
  #if test $(grep el6.x86 /boot/grub/grub.conf| grep -c huge) = 0 ; then
  # test if huge page defined as never in grub 
  if test $(grep el6.x86 /boot/grub/grub.conf| grep -vi title | grep -v initrd| grep -vc hugepage=never) != 0 ; then
	 #sed -i.pre-hadoop '/el6.x86/ s/$/ transparent_hugepage=never/' /boot/grub/grub.conf
	 sed  -i.pre-hadoop '/el6.x86/ s/luns=8192$/luns=8192 transparent_hugepage=never /' /boot/grub/grub.conf
     sed  -i '/el6.x86/ s/transparent_hugepage=.*/transparent_hugepage=never/' /boot/grub/grub.conf
	 #else
     #sed -i.pre-hadoop '/el6.x86/ s/transparent_hugepage=.*/transparent_hugepage=never/g' /boot/grub/grub.conf
  fi
  EOH
end
