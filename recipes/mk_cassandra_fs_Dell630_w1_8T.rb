#
# Cookbook Name:: hadoop
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
#  $(fdisk -l |grep sd |grep -E '800.2|480' |awk '{print $2}' |cut -d':' -f1)
#  $(fdisk -l | grep $mydisk | grep -c '800.2\|480')" -gt "0" ]
script 'cassandra uuid-fstab' do
interpreter "bash"
code <<-EOH

mydate=`date +%m%d%y_%H%M%S`
cp -p /etc/fstab /etc/fstab.$mydate

if [ "$(grep -c "/dblogs" /etc/fstab)" = "0" ]
then
  echo "myUU here I am" >> /tmp/dblogs.create
  mydisk=$(fdisk -l |grep sd |grep -E '800.2|480' |awk '{print $2}' |cut -d':' -f1)
  echo "Found dblog disk to be : $mydisk " >> /tmp/dblogs.create
  
  if [ "$(fdisk -l | grep $mydisk | grep -c -E '800.2|480')" -gt "0" ]
  then
    echo "running mkfs.xfs -f  $mydisk " >> /tmp/dblogs.create
    mkfs.xfs -f $mydisk  > /dev/null
    mkdir /dblogs
    echo "Created /dblogs " >> /tmp/dblogs.create
    myUU=$(blkid $mydisk | awk '{ print $2 }')
    printf "$myUU %s\t/dblogs \txfs\tnoatime,nodev\t0 0\n"  >> /etc/fstab
    echo "$myUU %s\t/dblogs \txfs\tnoatime,nodev\t0 0\n"  >> /tmp/dblogs.create
  else
    echo "could not get the count for disk : $mydisk" >> /tmp/dblogs.create
    echo "could not get the count for disk : $mydisk " 
  fi
mount -a
fi
   if [ "$(grep -c "/disk-" /etc/fstab)" = "0" ] ; then
       if [ "$(fdisk -l | grep /dev/sd | grep -c 1920)" -gt "1" ] ; then
          DISK=2
          for D in $(fdisk -l | grep /dev/sd | grep 1920 | awk '{ print $2 }'|cut -c -8| sort) ;do
                   mkfs.xfs -f $D  > /dev/null
                   UU=$(blkid $D | awk '{ print $2 }')
             DISK=$((DISK+1))
             if [ ! -d /disk-$DISK ] ; then
                mkdir  /disk-$DISK
             fi
             printf "%s\t/disk-%d \txfs\tnoatime,nodev\t0 0\n" "$UU" "$DISK"  >> /etc/fstab
          done
          mount -a
       fi
    fi
EOH
end
