#
# Cookbook Name:: hadoop
# Recipe:: jumbo_frame_fix.rb
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
ruby_block "Add entry MTU=9600 to ifcfg-bond0" do
  block do
    fe = Chef::Util::FileEdit.new("/etc/sysconfig/network-scripts/ifcfg-bond0")
    fe.insert_line_if_no_match(/MTU=9000/,			   
                               "MTU=9000")
    fe.write_file
  end
end
service 'network' do
  action [:restart]
end

