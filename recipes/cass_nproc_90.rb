template '/etc/security/limits.d/90-nproc.conf' do
  source '90-nproc.erb'
  mode '0644'
  owner 'root'
  group 'root'
end
