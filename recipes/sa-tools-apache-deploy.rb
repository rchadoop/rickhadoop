#
# Cookbook Name:: hadoop
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
bash 'apache-deploy' do
  user 'root'
  cwd '/root/bin'
  code <<-EOH
  cd /root/bin
  /root/bin/sa-tools apache-deploy
  EOH
end