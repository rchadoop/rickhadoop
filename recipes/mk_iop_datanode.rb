# Previously  mk_fs_hadoop.rb
# This recipe creates  /hadoop (100G), /usr/iop (20G), /var/iop (100G)
# This recipe is executed on Management Nodes Only
#
bash 'mk_hadoop_fs' do
  user 'root'
  cwd '/tmp'
  code <<-EOH



# ---------------------------------------------------------------------------------
   if [ $(awk '{ print $2 }'  /etc/fstab |grep -c \/usr/iop) = 0 ] ; then
      cd /root/bin
          /root/bin/sa-tools createfs-deploy -vg vgos -lv usriopvol -s 20 -m /usr/iop 
   fi

# ---------------------------------------------------------------------------------

   if [ $(awk '{ print $2 }'  /etc/fstab |grep -c \/opt/site1) = 0 ] ; then
      cd /root/bin
          /root/bin/sa-tools createfs-deploy -vg vgos -lv bigintvol  -s 800 -m /opt/site1
   fi
# ---------------------------------------------------------------------------------
   if [ $(awk '{ print $2 }'  /etc/fstab |grep -c \/var/iop) = 0 ] ; then
      cd /root/bin
          /root/bin/sa-tools createfs-deploy -vg vgos -lv variopvol  -s 100 -m /var/iop
   fi
# ---------------------------------------------------------------------------------
 if [ $(awk '{ print $2 }'  /etc/fstab |grep -c \/opt/voltage) = 0 ] ; then
      cd /root/bin
          /root/bin/sa-tools createfs-deploy -vg vgos -lv optvoltlv  -s 5 -m /opt/voltage
   fi
# ---------------------------------------------------------------------------------

  EOH
end


