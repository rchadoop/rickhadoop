# Cookbook Name::hadoop
# Recipe:: dsh_group

directory '/disk-3/data' do
  mode 0755
  owner 'cassandra'
  group 'cassandra'
  action :create
  recursive true
end

directory '/disk-4/data' do
  mode 0755
  owner 'cassandra'
  group 'cassandra'
  action :create
  recursive true
end

directory '/disk-5/data' do
  mode 0755
  owner 'cassandra'
  group 'cassandra'
  action :create
  recursive true
end

directory '/disk-6/data' do
  mode 0755
  owner 'cassandra'
  group 'cassandra'
  action :create
  recursive true
end

directory '/dblogs' do
  mode 0755
  owner 'cassandra'
  group 'root'
  #action :create
end


