# Cookbook Name::hadoop
# Recipe:: dsh_group
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
# This recipe create 3 files contating lists of servers
# datahosts mgmthosts and all hosts
directory '/etc/dsh/group' do
  mode 0755
  owner 'root'
  group 'root'
  action :create
  recursive true
end


template '/etc/yum.repos.d/epel.repo' do
  source 'epel.erb'
  mode '0644'
  owner 'root'
  group 'root'
end

template '/etc/yum.repos.d/epel-testing.repo' do
  source 'epel-testing.erb'
  mode '0644'
  owner 'root'
  group 'root'
end



