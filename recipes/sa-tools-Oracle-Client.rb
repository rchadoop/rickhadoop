#
# Cookbook Name:: hadoop
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
bash 'Oracle_Client-deploy' do
  user 'root'
  cwd '/root/bin'
  code <<-EOH
  if [ ! -x /opt/oracle/products/dbclient ] ; then
  cd /root/bin

    if [[ "$(uname -i)" =~ "64" ]]
    then
      /root/bin/sa-tools  oracle-client-deploy <<EOF
      64
      y
EOF
    fi

  fi
  EOH
end

