# Cookbook Name::hadoop
# Recipe:: test
#

mgmt_string = node['mgmt_access_conf_string']
original_file = '/etc/security/access.conf'
new_file = original_file + '.new'

File.open(new_file, 'w')  do |fo|
#  fo.puts 'hello'
current_env = node.chef_environment

#check if already added
#-----------------------------------------------------------------------------
unless File.readlines(original_file).grep(/^\+ : root : dc|^\+ : root : d2/).size > 0
uncommentedLine=0
File.open(original_file) do |f|
  f.each_line do |line|
    if line.match(/^#/) 
      fo.puts line
    else
       if uncommentedLine == 0
        uncommentedLine=1
        fo.puts "#Below added by Chef , contents from Attributes in  #{current_env} Environment, for Manamgement Node Access,and group access"
        fo.puts "+ : root : #{mgmt_string}"
        fo.puts "+ : (hdpsa) : ALL"
        fo.puts "+ : (ambops) : ALL"
        fo.puts line
       else
        fo.puts line
       end # end uncommented
    end # end line.match
  end #end do line
end  # end file.open

#-----------------------------------------------------------------------------


   unless uncommentedLine == 1  # means entire file begins with comments, so write at bottom
     #puts "UNNNNNNNNNNNNNNNNNN  COMMENTED   got it , uncommmented =#{uncommentedLine}"   
        fo.puts "#Below added by Chef , contents from Attributes in  #{current_env} Environment, for Manamgement Node Access,and group access"
        fo.puts "+ : root : #{mgmt_string}"
        fo.puts "+ : (hdpsa) : ALL"
        fo.puts "+ : (ambops) : ALL"
    end


else

new_string="+ : root : " + "#{mgmt_string}"
File.open(original_file) do |f|
  f.each_line do |myline|
    if myline.match(/^\+ : root : dc/)
	puts "THATTTT    LINE : mgmt string = #{mgmt_string}"
      if ( myline.chomp == "#{new_string}" )
        #  (line =~ /first_line/)
	puts "mgmt string in file equal to environment variable = #{mgmt_string}"
	fo.puts "+ : root : #{mgmt_string}"
      else
        fo.puts "#{new_string}"
	puts "Found New String Contents, replacing with : #{new_string}"
    end
      else
#	puts "NOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO : mgmt string = x#{mgmt_string}x"
#	puts "NOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO : news = x#{new_string}x"
#	puts "NOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO : line = x" + myline + "x"
        fo.puts myline
      end

#    end
#rrr
    #    fo.puts "+ : root : #{mgmt_string}"
    #else
    #  fo.puts line
    #   end # end uncommented
#    end # end line.match
  end #end do myline
end  # end file.open

#-----------------------------------------------------------------------------

  # puts "ALLLLLLLLLLLLLLLLLLLLLLL   READY    THERE  "
end  # end of the unless the rule is already there

File.rename(original_file, original_file + '.old')
File.rename(new_file, original_file)

end
