# Cookbook Name::hadoop
# Recipe:: dsh_group
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
# This recipe create 3 files contating lists of servers
# datahosts mgmthosts and all hosts
directory '/etc/dsh/group' do
  mode 0755
  owner 'root'
  group 'root'
  action :create
  recursive true
end


template '/etc/profile.d/pdsh.sh' do
  source 'pdsh.sh'
  mode '0644'
  owner 'root'
  group 'root'
end


