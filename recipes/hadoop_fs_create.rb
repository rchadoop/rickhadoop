#
# Cookbook Name:: hadoop
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
# create 100G /hadoop fs mgmt nodes only!
# 
# Hadoop /hadoop 100G Application Volume Group, file system on separate disk. Required for fsimage file
bash 'mk_hadoop_fs' do
  user 'root'
  cwd '/tmp'
  code <<-EOH
   if [ $(awk '{ print $2 }'  /etc/fstab |grep -c \/hadoop) = 0 ] ; then
      cd /root/bin
	  /root/bin/sa-tools createfs-deploy -vg vgapp -lv appvol -s 100 -m /hadoop
   fi
  EOH
end
