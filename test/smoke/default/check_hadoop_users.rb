
control 'Hadoop Users' do
  impact 0.3
  title 'Verify hadoop users'
  describe user('hadoop') do
    it { should exist }
    its('uid') { should eq 40000 }
    its('gid') { should eq 40000 }
    its('home') { should eq '/home/hadoop' }
    its('shell') { should eq '/bin/bash' }
  end
  describe user('hdfs') do
    it { should exist }
    its('uid') { should eq 40001 }
    its('gid') { should eq 40001 }
    its('home') { should eq '/home/hdfs' }
    its('shell') { should eq '/bin/bash' }
  end
  describe user('mapred') do
    it { should exist }
    its('uid') { should eq 40002 }
    its('gid') { should eq 40002 }
    its('home') { should eq '/home/mapred' }
    its('shell') { should eq '/bin/bash' }
  end
  describe user('kafka') do
    it { should exist }
    its('uid') { should eq 40003 }
    its('gid') { should eq 40003 }
    its('home') { should eq '/home/kafka' }
    its('shell') { should eq '/bin/bash' }
  end
  describe user('rrdcached') do
    it { should exist }
    its('uid') { should eq 40004 }
    its('gid') { should eq 40004 }
    its('home') { should eq '/home/rrdcached' }
    its('shell') { should eq '/bin/bash' }
  end
  describe user('zookeeper') do
    it { should exist }
    its('uid') { should eq 40005 }
    its('gid') { should eq 40005 }
    its('home') { should eq '/home/zookeeper' }
    its('shell') { should eq '/bin/bash' }
  end
  describe user('hbase') do
    it { should exist }
    its('uid') { should eq 40006 }
    its('gid') { should eq 40006 }
    its('home') { should eq '/home/hbase' }
    its('shell') { should eq '/bin/bash' }
  end
  describe user('hive') do
    it { should exist }
    its('uid') { should eq 40007 }
    its('gid') { should eq 40007 }
    its('home') { should eq '/home/hive' }
    its('shell') { should eq '/bin/bash' }
  end
  describe user('oozie') do
    it { should exist }
    its('uid') { should eq 40008 }
    its('gid') { should eq 40008 }
    its('home') { should eq '/home/oozie' }
    its('shell') { should eq '/bin/bash' }
  end
  describe user('ambari') do
    it { should exist }
    its('uid') { should eq 40009 }
    its('gid') { should eq 40009 }
    its('home') { should eq '/home/ambari' }
    its('shell') { should eq '/bin/bash' }
  end
  describe user('ambari-qa') do
    it { should exist }
    its('uid') { should eq 40010 }
    its('gid') { should eq 40010 }
    its('home') { should eq '/home/ambari-qa' }
    its('shell') { should eq '/bin/bash' }
  end
  describe user('ams') do
    it { should exist }
    its('uid') { should eq 40011 }
    its('gid') { should eq 40011 }
    its('home') { should eq '/home/ams' }
    its('shell') { should eq '/bin/bash' }
  end
  describe user('cassandra') do
    it { should exist }
    its('uid') { should eq 40012 }
    its('gid') { should eq 40012 }
    its('home') { should eq '/home/cassandra' }
    its('shell') { should eq '/bin/bash' }
  end
  describe user('flume') do
    it { should exist }
    its('uid') { should eq 40013 }
    its('gid') { should eq 40013 }
    its('home') { should eq '/home/flume' }
    its('shell') { should eq '/bin/bash' }
  end
  describe user('hcat') do
    it { should exist }
    its('uid') { should eq 40014 }
    its('gid') { should eq 40014 }
    its('home') { should eq '/home/hcat' }
    its('shell') { should eq '/bin/bash' }
  end
  describe user('knox') do
    it { should exist }
    its('uid') { should eq 40015 }
    its('gid') { should eq 40015 }
    its('home') { should eq '/home/knox' }
    its('shell') { should eq '/bin/bash' }
  end
  describe user('solr') do
    it { should exist }
    its('uid') { should eq 40016 }
    its('gid') { should eq 40016 }
    its('home') { should eq '/home/solr' }
    its('shell') { should eq '/bin/bash' }
  end
  describe user('spark') do
    it { should exist }
    its('uid') { should eq 40017 }
    its('gid') { should eq 40017 }
    its('home') { should eq '/home/spark' }
    its('shell') { should eq '/bin/bash' }
  end
  describe user('sqoop') do
    it { should exist }
    its('uid') { should eq 40018 }
    its('gid') { should eq 40018 }
    its('home') { should eq '/home/sqoop' }
    its('shell') { should eq '/bin/bash' }
  end
  describe user('yarn') do
    it { should exist }
    its('uid') { should eq 40019 }
    its('gid') { should eq 40019 }
    its('home') { should eq '/home/yarn' }
    its('shell') { should eq '/bin/bash' }
  end
  describe user('postgres') do
    it { should exist }
    its('uid') { should eq 40020 }
    its('gid') { should eq 40020 }
    its('home') { should eq '/var/lib/pgsql' }
    its('shell') { should eq '/bin/bash' }
  end
  describe user('ranger') do
    it { should exist }
    its('uid') { should eq 40021 }
    its('gid') { should eq 40021 }
    its('home') { should eq '/home/ranger' }
    its('shell') { should eq '/bin/bash' }
  end
end



