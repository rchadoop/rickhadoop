title 'umask check'
control 'umask_check-1.0' do
  impact 0.3
  title 'Verify umask set to 022'
  desc 'umask should be set to 022'
  describe file("/etc/login.defs") do
    its("content") { should match /UMASK           022/ }
  end
end

control 'umask check-1.1' do
  impact 0.3
  title 'Verify umask set to 022'
  desc 'umask should be set to 022'
  describe file("/etc/bashrc") do
    its("content") { should match /umask 022/ }
  end
end

