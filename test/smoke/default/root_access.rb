control 'Root_Access-1.0' do
  impact 0.3
  title 'Verify Root Access in sshd.conf'
  desc 'Checking Root Allowed Login in sshd_config'
  describe file("/etc/ssh/sshd_config") do
    its("content") { should match /PermitRootLogin without-password/ }
  end
end

control 'Root_Access-1.1' do
  impact 0.3
  title 'Verify Root Access in access.conf'
  desc 'Checking Root Allowed access from Management Server in access.conf'
  #describe file("/etc/security/access.conf") do
  describe file("/tmp/access.conf") do
    its("content") { should match /\+ \: root \: dc/ }
    its("content") { should_not match /\+ \: ALL \: ALL/ }
  end
end

control 'Root_Access-1.2' do
  impact 0.3
  title 'Verify authorized_keys exists'
  desc 'Checking authorized_keys exists for root'
  describe file("/root/.ssh/authorized_keys") do
    it { should exist}
  end
end
