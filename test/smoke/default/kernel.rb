title 'Verify Kernel Parameters'
control 'Kernel-1.0' do
  impact 0.3
  title 'Check net.ipv4.tcp_max_syn_backlog'
  desc 'net.ipv4.tcp_max_syn_backlog should be 8192'
  describe kernel_parameter('net.ipv4.tcp_max_syn_backlog') do
    its('value') { should eq 8192 }
      end
end

title 'Verify Kernel Parameters'
control 'Kernel-1.1' do
  impact 0.3
  title 'Disable IP V6'
  desc 'net.ipv6.conf.all.disable_ipv6 should be 1'
  describe kernel_parameter('net.ipv6.conf.all.disable_ipv6') do
    its('value') { should eq 1 }
      end
end

title 'Verify Kernel Parameters'
control 'Kernel-1.2' do
  impact 0.3
  title 'Disable IP V6'
  desc 'net.ipv6.conf.default.disable_ipv6 should be 1'
  describe kernel_parameter('net.ipv6.conf.default.disable_ipv6') do
    its('value') { should eq 1 }
      end
end

title 'Verify Kernel Parameters'
control 'Kernel-1.3' do
  impact 0.3
  title 'Disable IP V6'
  desc 'net.ipv6.conf.lo.disable_ipv6 should be 8192'
  describe kernel_parameter('net.ipv6.conf.lo.disable_ipv6') do
    its('value') { should eq 1 }
      end
end

