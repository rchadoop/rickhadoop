#@hadoop hard nofile 65536
#@hadoop soft nofile 65536
#@hadoop hard nproc 65536
#@hadoop soft nproc 65536

title 'limits.conf check'
control 'limits_conf-1.0' do
  impact 0.3
  title 'Verify hadoop hard nofile set to 65536'
  desc 'Verify hadoop hard nofile set to 65536'
  describe file("/etc/security/limits.conf") do
    its("content") { should match /^@hadoop hard nofile 65536/ }
  end
end

#control 'limits_conf-1.1' do
#  impact 0.3
#  title 'Verify umask set to 022'
#  desc 'umask should be set to 022'
#  describe file("/etc/bashrc") do
#    its("content") { should match /umask 022/ }
#  end
#end

