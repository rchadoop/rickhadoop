title 'dsh dir check /etc/dsh/group '
control 'pdsh-1.0' do
  impact 0.3
  title 'check /etc/dsh/group exists'
  desc '/etc/dsh/group must exist'
  describe file('/etc/dsh/group') do
    it { should be_directory }
    its('mode') { should eq 0755 }
    it { should be_owned_by 'root' }
  end
end

control 'pdsh-1.1' do
  impact 0.3
  title '/tmp/hadoop directory is owned by the root user'
  desc 'The /tmp/hadoop directory must be owned by the root user'
  describe file('/etc/profile.d/pdsh.sh') do
    it { should be_owned_by 'root' }
    its("content") { should match /export DSHGROUP_PATH=\/etc\/dsh\/group/ }
    its('mode') { should eq 0644 }
  end
end

