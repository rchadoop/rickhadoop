control 'sshd_conf-1.0' do
  impact 0.3
  title 'Verify MaxStartups sshd.conf'
  desc 'Checking MaxStartups equal 1024'
  describe file("/etc/ssh/sshd_config") do
    its("content") { should match /MaxStartups 1024/ }
  end
end

