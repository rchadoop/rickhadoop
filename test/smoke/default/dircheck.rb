title 'tmp_hadoop_dir'
control 'tmp-1.0' do
  impact 0.3
  title 'create /tmp/hadoop directory'
  desc 'a /tmp directory must exist'
  describe file('/tmp/hadoop') do
    it { should be_directory }
  end
end

control 'tmp-1.1' do
  impact 0.3
  title '/tmp/hadoop directory is owned by the root user'
  desc 'The /tmp/hadoop directory must be owned by the root user'
  describe file('/tmp/hadoop') do
    it { should be_owned_by 'root' }
  end
end
