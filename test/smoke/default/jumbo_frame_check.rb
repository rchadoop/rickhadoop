control 'Jumbo_Frame-1.0' do
  impact 0.3
  title 'Verify MTU set to 9000'
  desc 'Checking MTU setting = 9000 from file'
  describe file("/etc/sysconfig/network-scripts/ifcfg-bond0") do
    its("content") { should match /MTU=9000/ }
  end

end

control "Jumbo_Frame-1.1" do                      
  impact 1.0                                
  title "Verify bond0 ifconfig is MTU=9000"
  desc "MTU check for 9000 on bond0"
  describe command('/sbin/ifconfig bond0') do
   its('stdout') { should match /MTU:9000/ }
  end
end
